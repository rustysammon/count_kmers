#!/usr/bin/python3
'''
Author: Rusty Sammon
Purpose: This is interview question from Seven Bridges (https://www.sbgenomics.com/)

BACKGROUND
----------
FASTQ is a file format for storing biological sequences along with the estimated quality of each position, and
is a common output of DNA sequencing machines. In most cases, the lines are in sets of 4, with the first being
a sequence “ID”, the second being the sequence itself, the third containing just a ‘+’ character, and the fourth
containing quality information. More information and examples can be found at
https://en.wikipedia.org/wiki/FASTQ_format

A “k-­mer” is a substring of length k. For example, in the DNA sequence string “GATTACA”, we have the following
k-­mers of length 3: [“GAT”, “ATT”, “TTA”, “TAC”, “ACA”]. Counting k­-mers is a common subtask in many different
genomic applications.

TASK
----
Given a FASTQ file, produce a list, sorted by frequency, of the 25 most frequent DNA k-mers (a substring of
length k) of length 30 in that file. You are allowed to use libraries, as long as their purpose is not
specifically to count k-mers.

For reference and testing purposes, any FASTQ file from the 1000 Genomes project will serve as a valid example.
For instance, these are the FASTQ files available for HG01595. Note that we are only interested in the actual DNA
sequences stored in the second line of each entry. The sequences are independent and should not be concatenated.
ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase3/data/HG01595/sequence_read/

REQUIREMENTS
------------
The program must compile and run on Mac or Linux. If you’re on Windows, consider testing using a Linux
virtual machine. Virtualbox and Ubuntu are free.

MY SOLUTION
-----------
By default, the program will will find the 25 most frequent kmers of size k=30.  You can change these
values using the --kmersize and --topcount parameters.  You also need to select a --method, which is
discussed in more detail below:

If you're just counting kmers in a small file (less than 50MB), then there's no reason to get fancy.
You can use this basic hash-table method:

    python count_kmers.py -m naive -f YOUR_FASTQ_FILE

If you have a larger file and need to get exact counts of the number of kmers, then the "melsted" method is
much more memory efficient:

    python count_kmers.py -m melsted -f YOUR_FASTQ_FILE

I also wrote up a variation of melsted called "melsted1pass".  This method cuts the running time by
40-50% relative to melsted, but has a slim chance (currently 1%, adjustable in code but not in command
line) of overcounting some kmers by 1:

    python count_kmers.py -m melsted -f YOUR_FASTQ_FILE

'''

import sys, getopt, argparse, os
from readfq import readfq
from collections import deque, Counter
from timeit import timeit
import statistics
from pybloomfilter import BloomFilter


def fastq_file_overview(file):
    '''
    For a given FASTQ file, print some statistics about the sequences that are contained in the file.
    '''
    file.seek(0)
    sequence_lengths = [len(sequence) for name, sequence, quality in readfq(file)]
    file.seek(0)
    print('File "{}" contains {} sequences\n  avg length = {}\n  median = {}\n  stddev = {}\n'
          .format(file.name, len(sequence_lengths), statistics.mean(sequence_lengths), statistics.median(sequence_lengths),
                  statistics.stdev(sequence_lengths)))

def kmer_generator(k, sequence):
    '''
    This generator function will return all the kmers of size k in the sequence.

    If the sequence includes the letter N (representing "any nucleotide") or any other character
    other than A, G, C, T, or U then we assume that this was an error in sequencing.
    Any kmers that would include this non-conforming letter are omitted.

    For more details about letters that may appear in a sequence, see:
    https://en.wikipedia.org/wiki/Nucleic_acid_notation

    :param k: the length of each kmer.  Should be > 1
    :param sequence: the DNA sequence.  Should hav len(sequence) >= k
    :return kmers from the sequence
    '''
    if len(sequence) < k:
        # The sequence is too short to contain the kmer
        return

    # Omit kmers that contain letters outside of this list
    permissible_bases = {'A','G','C','T','U'}

    # Go through the sequence and generate each of the kmers
    kmer = deque(maxlen=k)
    for base in sequence.upper():
        if base in permissible_bases:
            kmer.append(base)
            if len(kmer) == k:
                yield ''.join(kmer)
                kmer.popleft()
        else:
            # This is an N or some other sequencing error
            kmer.clear()

def estimate_num_kmers(file):
    '''
    Make a conservative (ie, on the high side) guess at the number of unique kmers in a particular file,
    based on the size of the file.

    :param file: A FASTQ file containing DNA sequences.
    :return: an estimate of the number of unique kmers in the file.
    '''
    file_size = os.stat(file.fileno()).st_size
    # In my testing on small files, I saw a ratio of 3.7-4.0 bytes per unique km
    num_kmers = file_size / 3.5
    print("File size = {} bytes.  Expecting up to {} unique kmers".format(file_size, round(num_kmers)))
    return num_kmers


def count_kmers(file, counter, kmersize):
    '''
    For the given FASTQ file, count the number of kmers of length kmersize.
    This function counts all kmers using a "naive" Counter.  It runs into problems on long
    files where there are lots of point mutations and/or sequencing errors that create lots of
    kmers that occur only once.

    :param file: A FASTQ file containing DNA sequences.
    :param counter: a Counter object.  keys = kmer strings.  Once count_kmers is complete,
        you can query this counter to determin the most common kmers.
    :param kmersize: the number of letters/bases in the each kmer.  Should be > 1
    '''
    for name, sequence, quality in readfq(file):
        counter.update(kmer_generator(kmersize, sequence))


def melsted_count_kmers(file, counter, kmersize, two_pass=True, error_rate=0.01):
    '''
    This method of counting kmers uses a 2-pass approach, as suggested by:
    http://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-333

    The general idea is:
        1st Pass - Identify the kmers that appear at least twice in file
        2nd Pass - Count frequencies of the kmers that appear at least twice
    By ignoring kmers that occur only once, the 2nd Pass uses less memory because there are fewer kmers to count

    :param file: A FASTQ file containing DNA sequences.
    :param counter: a Counter object.  keys = kmer strings.  Once count_kmers is complete,
        you can query this counter to determin the most common kmers.
    :param kmersize: the number of letters/bases in the each kmer.  Should be > 1
    :param two_pass: True means that the normal 2 passes will be used, which guarantees the correct
        result, but takes double the time.
        False means that only one pass will be used (taking a little more than half the time), which
        may result in overcounting when there are collisions in the Bloom filter.  The overcounting
        will occur at error_rate.
    :param error_rate: The error_rate for the Bloom filter.
    '''

    # Take a guess at the number of kmers based on the size of the file
    capacity = estimate_num_kmers(file)

    # 1st Pass - Identify the kmers that appear at least twice in file
    file.seek(0)
    filter = BloomFilter(capacity=capacity, error_rate=error_rate)
    bloom_count = 0  # total kmers found with bloom filter
    for name, sequence, quality in readfq(file):
        for kmer in kmer_generator(kmersize, sequence):
            if filter.add(kmer):
                # This is the second time we've encountered this kmer
                # Add it to our list of kmers that occur multiple times
                if two_pass:
                    counter[kmer] = 0
                else:
                    if kmer in counter:
                        counter[kmer] += 1
                    else:
                        # Note that this initial counter value will be off by 1 if there are collisions in the Bloom filter
                        counter[kmer] = 2
            else:
                # This is a new kmer that we haven't seen before
                # Print some progress info
                bloom_count += 1
                if bloom_count % 10000 == 0:
                    percent = 100 * bloom_count / capacity
                    sys.stdout.write('\rFound {} unique kmers ({:0.0f}% of capacity) and {} kmers that occur at least twice.'
                                     .format(bloom_count, percent, len(counter)))
                    sys.stdout.flush()

    if two_pass:
        # 2nd Pass - Count frequencies of the kmers that appear at least twice
        print('\nStarting 2nd pass with {} kmers that occur multiple times'.format(len(counter)))
        file.seek(0)
        for name, sequence, quality in readfq(file):
            for kmer in kmer_generator(kmersize, sequence):
                if kmer in counter:
                    counter[kmer] += 1
    else:
        # The approximate counter value from the first pass is good enough
        print('\n')
        pass


def main(argv):
    # Parse the command line arguments
    parser = argparse.ArgumentParser(description=
        'This program will read DNA sequences from a FASTQ file and find the most frequent sequences (k-mers) '
        'of a given length.  For example, the command line:\n'
        '   count_kmers.py --filename big.fastq --kmersize 30 --topcount 25\n'
        'will print a list, sorted by frequency, of the 25 most frequent DNA k-mers of length 30 in that file.')
    parser.add_argument('-f', '--filename', metavar='f', required=True,
                        help='The input file, which should be in FASTQ format.  DNA sequences are read from this file')
    parser.add_argument('-k', '--kmersize', type=int, default=30,
                        help='The length in of the DNA substring that should be searched for.'
                        'For example, in the DNA sequence string “GATTACA”, we have the following k-­mers '
                        'of length 3: [“GAT”, “ATT”, “TTA”, “TAC”, “ACA”].')
    parser.add_argument('-t', '--topcount', type=int, default=25,
                        help='How many of the top k-mers should be reported. Ex: topcount=25 prints the 25 most '
                             'common k-mers.')
    parser.add_argument('-m', '--method', type=str, default='naive',
                        help='What method should be used when counting kmers?  Options are:\n'
                             '  naive - Just a simple hash table that stores the count of each kmer.\n'
                             '          This works well for small files, but can run out of memory when there\n'
                             '          are a large number of unique kmers.\n'
                             '  melsted - This method reduces memory usage by filtering out kmers that occur only once.\n'
                             '  melsted1pass - This method runs in a little more than half the time of bloom, but may have counts\n'
                             '          that are off by 1.  The error rate should be very low (and is customizable in the code)')
    args = parser.parse_args()
    print(args)

    dict = {} # Stores the count of each k-mer

    try:
        counter = Counter()  # the tally of each kmer

        # Read kmers from the file
        with open(args.filename, 'rt') as fp:
            if args.method == 'naive':
                duration = timeit(lambda: count_kmers(fp, counter, args.kmersize), number=1)
            elif args.method == 'melsted':
                duration = timeit(lambda: melsted_count_kmers(fp, counter, args.kmersize, two_pass=True), number=1)
            elif args.method == 'melsted1pass':
                duration = timeit(lambda: melsted_count_kmers(fp, counter, args.kmersize, two_pass=False), number=1)
            else:
                print('Method "{}" not recognized'.format(args.method))
                sys.exit(1)

            print('Search took {:0.2f} sec and found {} unique kmers'.format(duration, len(counter)))
            print('The {} most common kmers of length {} are:'.format(args.topcount, args.kmersize))
            for kmer, count in counter.most_common(args.topcount):
                print('   ', kmer, ' ', count)

    except IOError as e:
        print('Error reading from file: ', e)

if __name__ == "__main__":
    main(sys.argv[1:])