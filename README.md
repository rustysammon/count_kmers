PURPOSE
-------
This is interview question from Seven Bridges (https://www.sbgenomics.com/)

BACKGROUND
----------
FASTQ is a file format for storing biological sequences along with the estimated quality of each position, and
is a common output of DNA sequencing machines. In most cases, the lines are in sets of 4, with the first being
a sequence “ID”, the second being the sequence itself, the third containing just a ‘+’ character, and the fourth
containing quality information. More information and examples can be found at

https://en.wikipedia.org/wiki/FASTQ_format

A “k-­mer” is a substring of length k. For example, in the DNA sequence string “GATTACA”, we have the following
k-­mers of length 3: [“GAT”, “ATT”, “TTA”, “TAC”, “ACA”]. Counting k­-mers is a common subtask in many different
genomic applications.

TASK
----
Given a FASTQ file, produce a list, sorted by frequency, of the 25 most frequent DNA k-mers (a substring of
length k) of length 30 in that file. You are allowed to use libraries, as long as their purpose is not
specifically to count k-mers.

For reference and testing purposes, any FASTQ file from the 1000 Genomes project will serve as a valid example.  For instance, these are the FASTQ files available for HG01595. Note that we are only interested in the actual DNA sequences stored in the second line of each entry. The sequences are independent and should not be concatenated.

[ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase3/data/HG01595/sequence_read/]()

The program must compile and run on Mac or Linux. If you’re on Windows, consider testing using a Linux
virtual machine. Virtualbox and Ubuntu are free.

SETUP
-----
This is a Python 3 script.  I've tested it on Python 3.5.2.

I used a couple of libraries, one of which is included with code and the other you need to install yourself.

    pip install -r requirements.txt

MY SOLUTION
-----------
By default, the program will will find the 25 most frequent kmers of size k=30.  You can change these
values using the *--kmersize* and *--topcount* parameters.  You also need to select a *--method*, which is
discussed in more detail below:

If you're just counting kmers in a small file (less than 50MB), then there's no reason to get fancy.
You can use this basic hash-table method:

    python count_kmers.py -m naive -f YOUR_FASTQ_FILE

If you have a larger file and need to get exact counts of the number of kmers, then the "melsted" method is
much more memory efficient:

    python count_kmers.py -m melsted -f YOUR_FASTQ_FILE

I also wrote up a variation of melsted called "melsted1pass".  This method cuts the running time by
40-50% relative to melsted, but has a slim chance (currently 1%, adjustable in code but not in command
line) of overcounting some kmers by 1:

    python count_kmers.py -m melsted -f YOUR_FASTQ_FILE
    
See the code for details about each of these methods.