'''
This code was downloaded from:
https://github.com/lh3/readfq

Readfq is a collection of routines for parsing the FASTA/FASTQ format. It
seamlessly parses both FASTA and multi-line FASTQ with a simple interface.

Readfq was first implemented in a single C header file and then ported to Lua,
Perl and Python as a single function less than 50 lines. For users of scripting
languages, I encourage to copy-and-paste the function instead of using readfq
as a library. It is always good to avoid unnecessary library dependencies.

Readfq also strives for efficiency. The C implementation is among the fastest
(if not the fastest). The Python and Perl implementations are several to tens
of times faster than the official Bio* implementations. If you can speed up
readfq further, please let me know. I am not good at optimizing programs in
scripting languages. Thank you.

As to licensing, the C implementation is distributed under the MIT license.
Implementations in other languages are released without a license. Just copy
and paste. You do not need to acknowledge me. The following shows a brief
example for each programming language:

  # Python: generator function
  for name, seq, qual in readfq(sys.stdin): print seq

'''

def readfq(fp): # this is a generator function
    last = None # this is a buffer keeping the last unprocessed line
    while True: # mimic closure; is it a bad idea?
        if not last: # the first record or a record following a fastq
            for l in fp: # search for the start of the next record
                if l[0] in '>@': # fasta/q header line
                    last = l[:-1] # save this line
                    break
        if not last: break
        name, seqs, last = last[1:].partition(" ")[0], [], None
        for l in fp: # read the sequence
            if l[0] in '@+>':
                last = l[:-1]
                break
            seqs.append(l[:-1])
        if not last or last[0] != '+': # this is a fasta record
            yield name, ''.join(seqs), None # yield a fasta record
            if not last: break
        else: # this is a fastq record
            seq, leng, seqs = ''.join(seqs), 0, []
            for l in fp: # read the quality
                seqs.append(l[:-1])
                leng += len(l) - 1
                if leng >= len(seq): # have read enough quality
                    last = None
                    yield name, seq, ''.join(seqs); # yield a fastq record
                    break
            if last: # reach EOF before reading enough quality
                yield name, seq, None # yield a fasta record instead
                break

if __name__ == "__main__":
    import sys
    n, slen, qlen = 0, 0, 0
    for name, seq, qual in readfq(sys.stdin):
        n += 1
        slen += len(seq)
        qlen += qual and len(qual) or 0
    print(n, '\t', slen, '\t', qlen)
